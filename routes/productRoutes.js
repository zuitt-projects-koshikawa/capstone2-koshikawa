const express = require('express');
const router = express.Router();

// Import Controllers
// Product
const productControllers = require('../controllers/productControllers');


// Auth
const auth = require('../auth');
const {verify, verifyAdmin} = auth;







// Product Registration
router.post('/registerProduct',verify, verifyAdmin, productControllers.registerProducts);

// Get All Products
router.get('/getAllProduct', productControllers.getAllProduct);

// Get Product By Given Keyword
router.get('/getProductByKeyword', productControllers.getProductByKeyword);

// Update Product Details
router.put('/updateProduct/:id', verify, verifyAdmin, productControllers.updateProduct);

// Archive Product
router.put('/archiveProduct', verify, verifyAdmin, productControllers.archiveProduct);

// Activate Product
router.put('/activateProduct', verify, verifyAdmin, productControllers.activateProduct);

// Delete Product By Name
router.delete('/deleteProduct', verify, verifyAdmin, productControllers.deleteProduct);

// Delete All Product 
// router.delete('/deleteAllProduct', verify, verifyAdmin, productControllers.deleteAllProduct)

module.exports = router;