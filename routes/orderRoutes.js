const express = require('express');
const router = express.Router();

// Import Controllers
	// User
const userControllers = require('../controllers/userControllers');
	// Product
const productControllers = require('../controllers/productControllers');
	// Order
const orderControllers = require('../controllers/orderControllers');


// Auth
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// Add Order Items By Id
router.post('/addOrderById',verify, orderControllers.addOrderById);

// View All Orders Admin
router.get('/viewAllOrders',verify, verifyAdmin, orderControllers.viewAllOrders);

// View My Orders User
router.get('/viewMyOrder/:id',verify, orderControllers.viewMyOrder)

// Delete My Orders User
router.delete('/deleteMyOrder/:id',verify, orderControllers.deleteMyOrder)

module.exports = router;