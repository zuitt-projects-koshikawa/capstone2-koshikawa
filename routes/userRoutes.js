const express = require('express');
const router = express.Router();


// Import controllers
// User
const userControllers = require('../controllers/userControllers');

// Auth
const auth = require('../auth')
const {verify, verifyAdmin} = auth
// Routes

// User Registration
router.post('/signup', userControllers.registerUser);

// Login

router.post('/login', userControllers.loginUser);

// Get All Users Admin
router.get('/getAllUsers',verify, verifyAdmin, userControllers.getAllUsers);

// Get A Single User Admin
router.get('/getSingleUser/:id',verify, verifyAdmin, userControllers.getSingleUser);

// Updating A Regular User To Admin
router.put('/updateToAdmin/', verify, verifyAdmin, userControllers.updateToAdmin);
// Remove An Admin
router.put('/removeAdmin', verify, verifyAdmin, userControllers.removeAdmin);

// Update User Details
router.put('/updateUserDetails', verify, userControllers.updateUserDetails)

// Delete User By Id
router.delete('/deleteUser/:id', verify, verifyAdmin, userControllers.deleteUserById);

module.exports = router;