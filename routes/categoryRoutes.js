const express = require('express');
const router = express.Router();

// Import Controllers
// Category
const categoryControllers = require('../controllers/categoryControllers');

// Auth
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// Add Category
router.post('/addCategory', verify, verifyAdmin, categoryControllers.addCategory);

// Search By Category
router.put('/searchByCategory', categoryControllers.searchByCategory);

module.exports = router;