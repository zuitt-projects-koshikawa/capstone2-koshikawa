const mongoose = require('mongoose');
const productSchema = new mongoose.Schema ({
	productName: {
		type: String,
		required: [true, "Product Name is Required"],
		lowercase: true

	},

	productDetail: {
		type: String,
		required: [true, "Product Detail is Required"]
	},

	productQuantity: {
		type: Number,
		required: [true, "Quantity of Products is Required"],
		min: [1, 'Must be at least 1, got {VALUE}'],
		validate: { 
			validator: value => value % 1 === 0,
			message: num => `${num.value} is not a valid number.`
		}
	},

	/*category: {
		type: string,
		default: false
	},*/

	
	retailPrice: {
		type: Number,
		required: [true, "Retail Price is Required"],
		min: [1, 'Must be at least 1, got {VALUE}'],
	},

	discount: {
		type: Boolean,
		default: false
	},

	dateEnrolled: {
		type: Date,
		default: new Date()
	},
	
	isActive: {
		type: Boolean,
		default: true
	}
	
});

module.exports = mongoose.model("Product", productSchema)