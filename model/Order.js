const mongoose = require('mongoose');
const User = require('./User');
const Product = require('./Product');
const orderSchema = new mongoose.Schema ({

	userId: {
		type: mongoose.Schema.Types.ObjectId,
		required: true,
		// ref: 'User'
	},	

	username: {
		type: String,
		required: true,
		// ref: 'User'
	},	


	productId: {
		type: mongoose.Schema.Types.ObjectId,
		required: [true, "Quantity is Required"],
		// ref: 'Product'
	},

	productName: {
		type: String,
		required: [true, "Quantity is Required"],
		lowercase: true
	},

	orderQuantity: {
		type: String,
		required: [true, "Quantity is Required"],
		min: [1, 'Must be at least 1, got {VALUE}'],
		validate: { 
			validator: value => value % 1 === 0,
			message: num => `${num.value} is not a valid number.`
		}


	},

	status: {
		type: String,
		enum: ["pending", "delivered", "cancelled"],
		default: "pending"
	},
	

	totalPrice: {
		type: Number,
		req: true
	},

	dateOrder: {
		type: Date,
		default: new Date(),
		required: true
	},	

	pickUpDate: {
		type: Date,
		default: new Date(),
		required: true
	}


});

module.exports = mongoose.model("Order", orderSchema)