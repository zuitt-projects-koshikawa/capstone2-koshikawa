const mongoose = require('mongoose');
const userSchema = new mongoose.Schema ({
	firstName: {
		type: String,
		required: [true, "First Name is Required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is Required"]
	},

	email: {
		type: String,
		required: [true, "Email is Required"],
		unique: true,
		trim: true,
		lowercase: true
	},

	username: {
		type: String,
		required: [true, "Username is Required"],
		unique: true,
	},
	
	password: {
		type: String,
		required: [true, "Password is Required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile Number is Required"]
	},

	address: {
		type: String,
		required: [true, "Adress is Required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},	
});

module.exports = mongoose.model("User", userSchema)