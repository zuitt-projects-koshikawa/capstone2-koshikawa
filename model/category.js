const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  petCategory: { type: String, required: true }, /*dogs, cats, */
  type: { type: String }, /*Apparel, Supplements, foods, cleaning agents, medicine*/
});

exports.Category = mongoose.model("Category", categorySchema);