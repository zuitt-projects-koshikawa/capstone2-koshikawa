// Import bcrypt
const bcrypt = require('bcrypt');

// Import models
	//User 
const User = require('../model/User');
	//Product 
const Product = require('../model/Product');
	//Order 
const Order = require('../model/Order');

// Import auth
const auth = require('../auth');


// Add Order Items By Id

// module.exports.addOrderById= (req, res) => {
// 	// console.log(req.user)
// 	// console.log(req.params.id)


module.exports.addOrderById = (req, res) => {
if(req.user.isAdmin)
	return res.send("Action Forbidden")

	Product.findById(req.body.productId)
	.then(foundProduct=> {
		
		let orderQuantity = req.body.orderQuantity
		let productPrice = foundProduct.retailPrice;
		let totalPrice = orderQuantity * productPrice;

		let newOrder = new Order ({
			userId: req.user.id,
			username: req.user.username,
			productId: req.body.productId,
			productName: foundProduct.productName,
			orderQuantity: req.body.orderQuantity,
			totalPrice: totalPrice
		})
	order = newOrder.save();
	if (!order) 
		return res.status(404).send("The order cannot be created")
		res.send(newOrder);
	})
	
}

// View All Orders Admin
module.exports.viewAllOrders = (req, res) => {
	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// View My Order User
module.exports.viewMyOrder = (req, res) => {
	Order.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));	
}



// View My Orders User
module.exports.viewMyOrders = (req, res) => {
	Order.findOneById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Delete My Orders User
module.exports.deleteMyOrder = (req, res) => {
	Order.deleteMany({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


