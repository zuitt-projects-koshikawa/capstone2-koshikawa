// Import bcrypt
const bcrypt = require('bcrypt');

// Import email validator
const validator = require("email-validator")

// Import models
	//User 
const User = require('../model/User');

// Import auth
const auth = require('../auth');



// User registration

module.exports.registerUser = async (req, res) => {
	const {firstName, lastName, email, username, password, mobileNo, address} =req.body
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		username: req.body.username,
		password: hashedPW,
		mobileNo: req.body.mobileNo,
		address: req.body.address,
	});

	try {
		
		const Email = await User.findOne({email: req.body.email});
		const Username = await User.findOne({username: req.body.username});
		// console.log(req.body.username.length)
		// console.log(password.length)
		console.log(req.body.mobileNo.length)
		if (Email)
			return res.send ("This email isn't available. Please try another.");
		if(!validator.validate(req.body.email))
			return res.send ("Not a valid email. Please try another.");
		if (Username)
			return res.send ("This username isn't available. Please try another.");
		if (req.body.username.length < 6)
			return res.send ("This username is too short. Please try another.");
		if (password.length < 8)
			return res.send ("This password is too short. Please try another.");
		if (req.body.mobileNo.length != 11)
			return res.send ("Invalid mobile number. Please try another.");


		// SAVE USERNAME
		newUser.save()
		.then((result) => res.send(result))
	} catch (err) {res.send(err)}
};

// Login

module.exports.loginUser = async(req, res) => {
	let username = req.body.username,
	  password = req.body.password
	  email = req.body.email;
	
	let conditions = !!username ? {username:username} : {email:email};
	User.findOne(conditions)
	.then(foundUser => {

		if(foundUser === null){
			return res.send("Account does not exist");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
				if (isPasswordCorrect){
					return res.send ({accessToken: auth.createAccessToken(foundUser)})
				} else {
					return res.send ("Password is incorrect")
				}
		} 
	})
	.catch(err => res.send(err));	
};

// Get All Users Admin

module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};
// Get A Single User Admin
module.exports.getSingleUser = (req, res) => {
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
	
}

// Updating A Regular User To Admin

module.exports.updateToAdmin = (req, res) => {
	
	let updates = {
		isAdmin: true
	}


	User.findByIdAndUpdate(req.body.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

};

// Remove An Admin

module.exports.removeAdmin = (req, res) => {
	
	let updates = {
		isAdmin: false
	}

	User.findByIdAndUpdate(req.body.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

};

// Update User Details
module.exports.updateUserDetails = (req, res) => {
	let updates = {
		firstName: req.body.firstName ,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		address: req.body.adress,

	}

	User.findByIdAndUpdate(req.user.id, updates, {new:true})
	.then(updates => res.send(updates))
	.catch(err => res.send(err));
};

// Delete User By Id
module.exports.deleteUserById = (req, res) => {
	User.deleteOne({_id: req.body.id})
	.then(result => res.send (result))
	.catch(err => res.send(err));
};


