const {Category} = require('../model/category');
const express = require('express');


module.exports.addCategory = async (req, res) => {
	let category = new Category ({
		petCategory: req.body.petCategory,
		type: req.body.type

	})

	category = await category.save();

	if(!category){
		return res.status(404).send("Invalid Category.")
	}
	res.send(category)
}

module.exports.searchByCategory = async (req, res) => {
	const categoryList = await Category.find();

	if (!categoryList) {
		res.status(500).json({success: false});
	}

	res.send(categoryList);
};







 