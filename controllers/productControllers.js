// Import bcrypt
	const bcrypt = require('bcrypt');

// Import models
	// Product
const Product = require('../model/Product');
	// User
const User = require('../model/User');

// Import auth
const auth = require('../auth');

// Product Registration
module.exports.registerProducts = async (req, res) => {

	let newProduct = new Product({

		productName: req.body.productName,
		productDetail: req.body.productDetail,
		productQuantity: req.body.productQuantity,
		retailPrice: req.body.retailPrice			
	});

	newProduct.save()
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Get All Products
module.exports.getAllProduct = (req, res) => {
	Product.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Get Product By Given Keyword
module.exports.getProductByKeyword = (req, res) => {
	Product.find({$or: [{productName: { $regex: req.body.productName, $options: 'i'}}]
	})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Update Product details
module.exports.updateProduct = (req, res) => {

	let updates = {
		productName: req.body.productName,
		productDetail: req.body.productDetail,
		productQuantity: req.body.productQuantity,
		retailPrice: req.body.retailPrice,
		discount: req.body.discount
	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// Archive Product By Name
module.exports.archiveProduct = (req, res) => {
	let updates = {
		isActive: false
	};

	Product.findOneAndUpdate({productName: req.body.productName}, updates, {new: true})
	.then(result => res.send (result))
	.catch(err => res.send(err));
};

// Activate Product By Name
module.exports.activateProduct = (req, res) => {
	let updates = {
		isActive: true
	};

	Product.findOneAndUpdate({productName: req.body.productName}, updates, {new: true})
	.then(result => res.send (result))
	.catch(err => res.send(err));
};

// Delete Product By Name
module.exports.deleteProduct = (req, res) => {
	Product.deleteOne({productName: req.body.productName})
	.then(result => res.send (result))
	.catch(err => res.send(err));
};

/*Delete All Items Password Required

module.exports.deleteAllProduct = (req, res) => {

	let password = req.body.password;

	User.findOne(req.user.id)
	.then(foundUser => {
		const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password)
		if (isPasswordCorrect){
			console.log(isPasswordCorrect)
			User.deleteMany()
			.then(result => res.send (result))
		} else {
			return res.send("Password is incorrect")
		}

	})
	.catch(err => res.send(err))
};*/
