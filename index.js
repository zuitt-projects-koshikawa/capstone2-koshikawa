const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const categoryRoutes = require('./routes/categoryRoutes');
const port = 4000;

const app = express();

mongoose.connect("mongodb+srv://admin_koshikawa:admin169@batch-169.df9yo.mongodb.net/capstone2Koshikawa?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));

db.once('open', () => console.log("Connected to MongoDB"));

// middlewares
app.use(express.json());
app.use(cors());


// routes

app.use('/users', userRoutes);
app.use('/products', productRoutes);
app.use('/order', orderRoutes);
app.use('/category', categoryRoutes);

app.listen(port, () => console.log(`Server is running at port:${port}`))
